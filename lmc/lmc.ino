// *** lmc ***

//
// Leipzig Motor Controller runs cheap chinese motor drivers from an Arduino uno
//
// copyright Helmut Fedder 12 July 2016, helmut@fedder.net, all rights reserved
//

#include <CmdMessenger.h>  // CmdMessenger

// sign function
template <typename T> int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

// pins
const int XP = 7;
//const int XLP = 8;
const int XLN = 8;
const int XD = 9;

const int YP = 11;
//const int YLP = 12;
const int YLN = 12;
const int YD = 13;

const int ZP = 3;
//const int ZLP = 4;
const int ZLN = 4;
const int ZD = 5;

// position registers
long x = 0; // current x position
long y = 0; // current y position
long z = 0; // current z position

// Attach a new CmdMessenger object to the default Serial port
CmdMessenger cmdMessenger = CmdMessenger(Serial);

// This is the list of recognized commands.
enum
{
  kStatus          , // Message to report the status
  kPosition        , // Message to return a position
  kGet             , // Command to get a position
  kSet             , // Command to set a position
  kGoTo            , // Command to go to a position
};

// Callbacks define on which received commands we take action
void attachCommandCallbacks()
{
  // Attach callback methods
  cmdMessenger.attach(OnUnknownCommand);
  cmdMessenger.attach(kGet, OnGet);
  cmdMessenger.attach(kSet, OnSet);
  cmdMessenger.attach(kGoTo, OnGoTo);
}

// Called when a received command has no attached function
void OnUnknownCommand()
{
  cmdMessenger.sendCmd(kStatus,"Command without attached callback");
}

// Callback function that returns a position
void OnGet()
{
  int axis = cmdMessenger.readInt16Arg();
  switch (axis)
  {
    case 0:
      cmdMessenger.sendCmd(kPosition,(long)x);
      break;
    case 1:
      cmdMessenger.sendCmd(kPosition,(long)y);
      break;
    case 2:
      cmdMessenger.sendCmd(kPosition,(long)z);
      break;
  }
}

// Callback function that sets a current position
void OnSet()
{
  // Read position argument, interpret string as integer
  // and set the position register
  int axis = cmdMessenger.readInt16Arg();
  long position = cmdMessenger.readInt32Arg();
  switch (axis)
  {
    case 0:
      x = position;
      break;
    case 1:
      y = position;
      break;
    case 2:
      z = position;
      break;
  }
  // Return the position register
  cmdMessenger.sendCmd(kPosition,(long)position);
}

void Pulse(int pin, int mu_sec)
{
    digitalWrite(pin,HIGH);
    delayMicroseconds(mu_sec);
    digitalWrite(pin,LOW);
    delayMicroseconds(mu_sec);
}

int PulseLength(long n)
{
    if (n<150) { return 1000; }
    else if (n<300) { return 500; }
    else if (n<700) { return 250; }
    else { return 100; }
}

long travel(long distance, int dir_pin, int pulse_pin, int neg_lim_pin, int pos_lim_pin)
{
    long steps = abs(distance);

    int direction = sign(distance);

    // set the directioon
    digitalWrite(dir_pin,(direction+1)/2); // ToDo: here we assume positive direction = High. is this correct ?
    delayMicroseconds(1000); // ToDo: is this long enough ?

    // ToDo: we should travel when the limit switch is pressed
    // and we are traveling in the right direction. Here
    // we assume that we have a negative limit switch.
    // is this correct ?


    // the actual travel
    long i;
    for (i=0; i<steps; i++)
    {
        // positive imit switch
        //if (digitalRead(pos_lim_pin) && direction > 0 ) { break; }

        // negative limit switch
        if (digitalRead(neg_lim_pin) && direction < 0 ) { break; }    
        int pulse_length = PulseLength(min(i,steps-i));
        Pulse(pulse_pin, pulse_length);
    }

    return direction*i;
}


// Callback function that moves to a position
void OnGoTo()
{
  // Read position argument, interpret string as integer
  int axis = cmdMessenger.readInt16Arg();
  long target = cmdMessenger.readInt32Arg();

  long distance;

  switch (axis)
  {
    case 0:
      distance = target - x;
      //set the new position
      x += travel(distance, XD, XP, XLN, 0);
      // return the actual position
      cmdMessenger.sendCmd(kPosition,(long)x);
      break;
    case 1:
      distance = target - y;
      //set the new position
      y += travel(distance, YD, YP, YLN, 0);
      // return the actual position
      cmdMessenger.sendCmd(kPosition,(long)y);
      break;
    case 2:
      distance = target - z;
      //set the new position
      z += travel(distance, ZD, ZP, ZLN, 0);
      // return the actual position
      cmdMessenger.sendCmd(kPosition,(long)z);
      break;
  }
}

// Setup function
void setup() 
{
  // Listen on serial connection for messages from the PC
  Serial.begin(115200); 

  // Adds newline to every command
  cmdMessenger.printLfCr();   

  // Attach my application's user-defined callback methods
  attachCommandCallbacks();

  // Send the status to the PC that says the Arduino has booted
  // Note that this is a good debug function: it will let you also know 
  // if your program had a bug and the arduino restarted  
  cmdMessenger.sendCmd(kStatus,"Arduino has started!");

}

// Loop function
void loop() 
{
  // Process incoming serial data, and perform callbacks
  cmdMessenger.feedinSerialData();
}



