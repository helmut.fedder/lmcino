This is LMCino, the Leipzig Motor Controller based on an arduino

Requirements

 * CmdMessenger arduino library (can be added from within the arduino IDE)
 * cmdmessenger.py (available here: https://github.com/Davideddu/python-cmdmessenger)
 * python 3
 * pyserial

Installation

 * connect an arduino rev 3, open the file lmc.ino in you arduino IDE
   and upload
 * create a directory and put the 3rd party file 'cmdmessenger.py' and
   the file lmcino.py into that folder
 * open a command terminal, change to that folder and enter 'ipython'
 * enter 'run lmcino'
 * start playing and inspecting, e.g. by typing lmc.set(0,1000)
 * read the source files lmcino.py and lmc.ino
   
