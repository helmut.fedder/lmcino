#!/usr/bin/python
#
# lmcino.py
#
# Leipzig Motor Controller runs cheap chinese motor drivers from an Arduino uno
#
# copyright Helmut Fedder 12 July 2016, helmut@fedder.net, all rights reserved
#

from time import sleep
from serial import Serial
from cmdmessenger import CmdMessenger

ids = {'Status':0,
       'Position':1,
       'Get':2,
       'Set':3,
       'GoTo':4,
       }

class LMC():

    def __init__(self, serial_port='/dev/ttyACM0'):
        self.serial_port = serial_port

        # open the serial device
        self.ser_dev = Serial(self.serial_port, 115200, timeout=0)

        # create the messenger
        self.messenger = CmdMessenger(self.ser_dev)
        self.messenger.attach(func=self.on_unknown_command)
        self.messenger.attach(func=lambda *a, **kw: None, msgid=ids['Position']) # do-nothing callback for position messages

        # fetch initial startup message
        msg_id, args = self.messenger.wait_for_ack(ids['Status'], timeout=10)
        print(args)

    def get(self, axis):
        self.messenger.send_cmd(ids['Get'], axis)
        msg = self.messenger.wait_for_ack(ids['Position'], timeout=1)
        print(int(msg[1]))

    def set(self, axis, pos):
        self.messenger.send_cmd(ids['Set'], axis, pos)
        msg = self.messenger.wait_for_ack(ids['Position'], timeout=1)
        print(int(msg[1]))

    def goto(self, axis, pos, timeout=60):
        self.messenger.send_cmd(ids['GoTo'], axis, pos)
        msg = self.messenger.wait_for_ack(ids['Position'], timeout=timeout)
        print(int(msg[1]))
        
    def on_unknown_command(received_command, *args, **kwargs):
        """Handle when an unknown command has been received.
        """
        print("Command without attached callback received")


#    def __del__(self):
#        self.close_serial()


if __name__ == '__main__':

    lmc = LMC()
    #lmc.set_x(12)

"""
def write(msg, *args):

def ask()

ser_dev = Serial('/dev/ttyACM0', 115200, timeout=0)

messenger = CmdMessenger(ser_dev)

messenger.attach(func=on_unknown_command)
messenger.attach(func=on_status, msgid=msg_ids['status'])

messenger.send_cmd(msg_ids['GetX'])
messenger.feed_in_data()
"""


